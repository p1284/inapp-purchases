Scriptable InApp Purchase Manager (Unity IAP plugin implemented), 
for all IAP(Google,Apple,Steam...) stores.
Has dependencies:

https://gitlab.com/p1284/gamereadypack.git

https://gitlab.com/p1284/game-cheats.git

https://gitlab.com/p1284/analytics-manager.git

License
MIT License

Copyright © 2021 Gameready
