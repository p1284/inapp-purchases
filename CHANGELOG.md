# Changelog
## [2.0.0] - 2023-09-10
### new implementation for VContainer and Gameready pack

## [1.0.1] - 2023-12-09
### add AnalyticsManager dependency
### improve editor scripts

## [1.0.0] - 2023-08-09
### InAppManager with UnityIAP implementation