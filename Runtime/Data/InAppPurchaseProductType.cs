﻿namespace GameReady.IAP.Data
{
    public enum InAppPurchaseProductType
    {
        Consumable,
        NonConsumable,
        Subscription
    }
}