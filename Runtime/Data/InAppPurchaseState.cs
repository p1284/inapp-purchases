﻿namespace GameReady.IAP.Data
{
    public enum InAppPurchaseState
    {
        PurchaseStarted = 0,
        PurchaseComplete,
        PurchaseFailed
    }
}