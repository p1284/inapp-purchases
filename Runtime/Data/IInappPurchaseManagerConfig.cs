using System;
using GameReady.Analytics.Data;
using GameReady.IAP.Validator;
using Gameready.Utils.Services.Core;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.IAP.Data
{
    public class IInappPurchaseManagerConfig : BaseServiceConfig
    {
        [SerializeField] private BaseIAPValidator _recieptValidator;

        [SerializeField] private InAppPurchaseProduct[] _products;

        [Header("ANALYTICS")] [SerializeField] private AnalyticEventKey _purchaseAnalyticEvent;

        public AnalyticEventKey PurchaseAnalyticEvent => _purchaseAnalyticEvent;

        public BaseIAPValidator RecieptValidator => _recieptValidator;

        public InAppPurchaseProduct[] Products => _products;

        public void Validate()
        {
            if (_products == null || _products.Length == 0) return;
            foreach (var purchaseProduct in _products)
            {
                if (string.IsNullOrEmpty(purchaseProduct.ID))
                {
                    throw new Exception("InAppPurchaseServiceConfig product id is empty");
                }

                if (string.IsNullOrEmpty(purchaseProduct.StoreID))
                {
                    throw new Exception("InAppPurchaseServiceConfig product StoreID is empty");
                }
            }

            Debug.Log($"{this.GetType().Name} Validation Complete!");
        }


        public override void RegisterService(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<IAPPurchaseManager>().As<IInAppPurchaseManager>().WithParameter(this);
        }
    }
}