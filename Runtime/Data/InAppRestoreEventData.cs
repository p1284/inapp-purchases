﻿using System.Collections.Generic;

namespace GameReady.IAP.Data
{
    public readonly struct InAppRestoreEventData
    {
        public readonly List<InAppPurchaseProduct> RestoredProducts;

        public InAppRestoreEventData(List<InAppPurchaseProduct> restoredProducts)
        {
            RestoredProducts = restoredProducts;
        }
    }
}