﻿namespace GameReady.IAP.Data
{
    public sealed class InAppPurchaseEventData
    {
        public readonly InAppPurchaseProduct PurchasedProduct;

        public readonly InAppPurchaseState State;
        
        public InAppPurchaseEventData(InAppPurchaseProduct purchasedProduct, InAppPurchaseState state)
        {
            PurchasedProduct = purchasedProduct;
            State = state;
        }
    }
}