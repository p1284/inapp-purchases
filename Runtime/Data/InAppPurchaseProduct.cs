﻿using System;
using UnityEngine;

namespace GameReady.IAP.Data
{
    [Serializable]
    public class InAppPurchaseProduct
    {
        public event Action<InAppPurchaseProduct> OnPurchased;

        public event Action<InAppPurchaseProduct> OnPurchaseStarted;

        public event Action<InAppPurchaseProduct> OnPurchaseFailed;

        [Tooltip("Unique id for get this product")] [SerializeField]
        private string _id;

        [SerializeField] private InAppPurchaseProductType _type;

        [SerializeField] private string _googlePlayStoreID;
        [SerializeField] private string _appStoreID;

        [SerializeField] private string _defaultPrice = "0.99$";

        [SerializeField] private string _titleLocaleId;

        [SerializeField] private string _payoutID;

        [NonSerialized] public string Placement;
        
        public string ID => _id;

        public InAppPurchaseProductType Type => _type;

        public string DefaultPrice => _defaultPrice;

        public string TitleLocaleId => _titleLocaleId;

        public string PayoutID => _payoutID;

        private int _purchasedCount;

        public string StoreID
        {
            get
            {
#if UNITY_ANDROID
                return _googlePlayStoreID;
#elif UNITY_IOS
                  return _appStoreID;
#endif
                return "";
            }
        }

        public int PurchasedCount => _purchasedCount;

        public bool IsPurchased => _purchasedCount > 0;

        internal void PurchaseFailed()
        {
            OnPurchaseFailed?.Invoke(this);
        }

        internal void PurchaseStarted()
        {
            OnPurchaseStarted?.Invoke(this);
        }

        internal void Purchase(int count)
        {
            _purchasedCount = count;
            OnPurchased?.Invoke(this);
        }

        public override string ToString()
        {
            return $"id:{_id} storeId:{StoreID} payout:{_payoutID} type:{_type}";
        }
    }
}