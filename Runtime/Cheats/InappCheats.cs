using System.Linq;
#if CHEATS
using Gameready.Cheats;
#endif
using UnityEngine;
using UnityEngine.UIElements;
using VContainer;

namespace GameReady.IAP.Cheats
{
#if CHEATS
    public class InappCheats : BaseUIToolkitCheat
    {
        private VisualElement _mainContainer;

        private DropdownField _productIdSelector;

        private Button _purchaseButton;
        private Button _restoreButton;

        private IInAppPurchaseManager _purchaseManager;

        protected override VisualElement BuildInternal()
        {
            _mainContainer = _template.Instantiate();

            _purchaseManager = Resolver.Resolve<IInAppPurchaseManager>();

            _productIdSelector = _mainContainer.Q<DropdownField>("product-id");

            var products = _purchaseManager.GetAllProducts().Select(product => product.ID).ToList();

            _productIdSelector.choices = products;
            _productIdSelector.SetValueWithoutNotify(products.FirstOrDefault());

            _purchaseButton = _mainContainer.Q<Button>("purchase");
            _restoreButton = _mainContainer.Q<Button>("restore");

            _purchaseButton.clicked += OnPurchaseClicked;
            _restoreButton.clicked += OnRestoreClicked;

            return _mainContainer;
        }

        public override void Dispose()
        {
            base.Dispose();
            if (_purchaseButton is not null)
            {
                _purchaseButton.clicked -= OnPurchaseClicked;
            }

            if (_restoreButton is not null)
            {
                _restoreButton.clicked -= OnRestoreClicked;
            }

            _mainContainer?.RemoveFromHierarchy();
        }


        private void OnPurchaseClicked()
        {
            var product =
                _purchaseManager.GetProduct(purchaseProduct => purchaseProduct.ID == _productIdSelector.value);
            if (product == null)
            {
                Debug.LogError($"Cheats Purchase Product:{_productIdSelector.value} not exist");
                return;
            }

            _purchaseManager.Purchase(product, "cheats_panel");
        }

        private void OnRestoreClicked()
        {
            _purchaseManager.Restore();
        }
    }
#endif
}