using System;
using GameReady.IAP.Data;

namespace GameReady.IAP
{
    public interface IInAppPurchaseManager
    {
        event Action<IAPPurchaseManager> OnInitialized;
        
        event Action<InAppPurchaseEventData> OnPurchaseResult;
        
        bool IsInitialized { get; }
        
        bool IsPurchased(string productId, out int count);

        InAppPurchaseProduct[] GetAllProducts();

        InAppPurchaseProduct[] GetProducts(Predicate<InAppPurchaseProduct> predicate);

        InAppPurchaseProduct GetProduct(Predicate<InAppPurchaseProduct> predicate);

        void Purchase(InAppPurchaseProduct product, string placement = "");

        void Purchase(string storeId, string placement = "");

        void Restore();
    }
}