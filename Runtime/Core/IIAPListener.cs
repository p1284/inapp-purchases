﻿using System;
using GameReady.IAP.Data;
using GameReady.IAP.Validator;

namespace GameReady.IAP.Core
{
    public interface IIAPListener
    {
        event Action<bool> OnInitialized;

        event Action<InAppPurchaseEventData> OnPurchaseComplete;

        void Initialize(InAppPurchaseProduct[] products, BaseIAPValidator validator);

        void Purchase(string storeId);

        void Restore();
    }
}