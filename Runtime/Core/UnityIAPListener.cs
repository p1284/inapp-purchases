﻿#if UNITY_IAP
using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameReady.IAP.Data;
using Gameready.IAP.Validator;
using GameReady.IAP.Validator;
using GameReady.IAP.Validator.Data;
using Unity.Services.Core;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;

namespace GameReady.IAP.Core
{
    public class UnityIAPListener : MonoBehaviour, IIAPListener, IDetailedStoreListener
    {
        public event Action<bool> OnInitialized;

        public event Action<InAppPurchaseEventData> OnPurchaseComplete;

        private IStoreController _controller;
        private IExtensionProvider _extensionProvider;
        
        private Dictionary<string, InAppPurchaseProduct> _productMap;

        private bool _purchaseStarted;

        private UnityIAPRecieptValidator _validator;

        private InAppPurchaseProduct[] _products;
        
        private void Awake()
        {
            _productMap = new Dictionary<string, InAppPurchaseProduct>();
            DontDestroyOnLoad(gameObject);
        }

        public void Initialize(InAppPurchaseProduct[] products,BaseIAPValidator validator)
        {
            if (_controller != null) return;
            _products = products;
            _validator = validator as UnityIAPRecieptValidator;
            StartInitAsync().Forget();
        }

   
        public void Purchase(string storeId)
        {
            if (_purchaseStarted || _controller == null) return;
            _purchaseStarted = true;

            var iapProduct = GetProduct(storeId);

            var product = _controller.products.WithID(storeId);

            if (product.definition.type == ProductType.Consumable ||
                product.definition.type == ProductType.Subscription)
            {
                if (product.hasReceipt)
                {
                    Debug.Log($"UnityIAP product:{product.definition.id} already bought");
                    SendPurchaseEvent(iapProduct, InAppPurchaseState.PurchaseStarted);
                    return;
                }
            }

            SendPurchaseEvent(iapProduct, InAppPurchaseState.PurchaseStarted);
            _controller.InitiatePurchase(storeId);
        }

        public void Restore()
        {
            if (_controller == null)
            {
                Debug.Log("UnityIAP is not initialized yet.");
                return;
            }

            Debug.Log("UnityIAP Trying to restore transactions...");

#if UNITY_IOS
            _extensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions((result, data) =>
            {
                Debug.Log($"UnityIAP Ios restore:{result} data:{data}");
            });
#elif UNITY_ANDROID
            _extensionProvider.GetExtension<IGooglePlayStoreExtensions>().RestoreTransactions((result, data) =>
            {
                Debug.Log($"UnityIAP Android restore:{result} data:{data}");
            });
#else
               Debug.Log("UnityIAP restore not implemented");
#endif
        }

        public void GetSubscriptionInfo()
        {
            
        }

        private async UniTaskVoid StartInitAsync()
        {
            try
            {
                await UnityServices.InitializeAsync();

                var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

                foreach (var product in _products)
                {
                    builder.AddProduct(product.StoreID, GetUnityIAPType(product.Type));
                }

                UnityPurchasing.Initialize(this, builder);
            }
            catch (Exception e)
            {
                OnInitializeFailed(InitializationFailureReason.PurchasingUnavailable);
            }
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.LogError($"UnityIAP init failed:{error}");
            OnInitialized?.Invoke(false);
        }

        void IStoreListener.OnInitializeFailed(InitializationFailureReason error, string message)
        {
            Debug.LogError($"UnityIAP init failed:{error} message:{message}");
            OnInitialized?.Invoke(false);
        }


        //when Restore will be invoked for any items the user already owns
        //fired for each Pending Purchase at app start
        //fired when Unity IAP receives a purchase which is then ready for local and server-side validation
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
        {
            if (!_purchaseStarted)
            {
                Debug.Log("UnityIAP ProcessPurchase autoRun, for Restore or Pending transactions");
            }

            var product = purchaseEvent.purchasedProduct;
            var iapProduct = GetProduct(product.definition.id);

            ValidationPurchaseState state = _validator == null
                ? ValidationPurchaseState.Purchased
                : _validator.ValidatePurchase(product);
            //handle what happens with the product next
            switch (state)
            {
                case ValidationPurchaseState.Purchased:
                    //nothing to do here: with the transaction finished at this point it means that either
                    //1) local validation passed but server validation is not supported, or
                    //2) validation is not supported at all, e.g. when running on a non-supported store
                    break;

                //transaction is pending or about to be validated on the server
                //it is important to return pending to leave the transaction open for the ReceiptValidator
                //the ReceiptValidator will fire its purchaseCallback when done processing
                case ValidationPurchaseState.Pending:
                    Debug.Log("UnityIAP Product purchase '" + product.definition.storeSpecificId + "' is pending.");
                    return PurchaseProcessingResult.Pending;

                //transaction invalid or failed locally. Complete transaction to not validate again
                case ValidationPurchaseState.Failed:
                    Debug.LogError("UnityIAP Product purchase '" + product.definition.storeSpecificId +
                                   "' deemed as invalid.");
                    break;
            }

            SendPurchaseEvent(iapProduct,
                state == ValidationPurchaseState.Purchased
                    ? InAppPurchaseState.PurchaseComplete
                    : InAppPurchaseState.PurchaseFailed);
            _purchaseStarted = false;
            return PurchaseProcessingResult.Complete;
        }

        void IStoreListener.OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            var iapProduct = GetProduct(product.definition.id);
            Debug.LogError($"UnityIAP purchase failed:{product.definition.id} reason:{failureReason}");
            SendPurchaseEvent(iapProduct, InAppPurchaseState.PurchaseFailed);
            _purchaseStarted = false;
        }

        void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _controller = controller;
            _extensionProvider = extensions; 

            if (_validator != null)
            {
                _validator.OnRemoteValidationComplete += OnRemoteValidationComplete;
                _validator.Initialize(_controller);
            }

            var products = _controller.products.all;

            //fill products
            foreach (var product in products)
            {
                var inappProduct = Array.Find(_products,
                    purchaseProduct => purchaseProduct.StoreID == product.definition.id);
                if (inappProduct != null)
                {
                    _productMap.Add(product.definition.id, inappProduct);
                }
            }

            OnInitialized?.Invoke(true);

            Debug.Log("UnityIAP Initialization complete!");
        }

        private void OnRemoteValidationComplete(ValidationRequestEventData data)
        {
            Debug.Log($"UnityIAP validation complete:{data.Response.ToString()}");
        }

        void IDetailedStoreListener.OnPurchaseFailed(Product product, PurchaseFailureDescription failureDescription)
        {
            Debug.LogError($"UnityIAP Purchase failed - Product: '{product.definition.id}'," +
                           $" Purchase failure reason: {failureDescription.reason}," +
                           $" Purchase failure details: {failureDescription.message}");
            var iapProduct = GetProduct(product.definition.id);
            SendPurchaseEvent(iapProduct, InAppPurchaseState.PurchaseFailed);
            _purchaseStarted = false;
        }

        private InAppPurchaseProduct GetProduct(string storeId)
        {
            return _productMap[storeId];
        }

        private void SendPurchaseEvent(InAppPurchaseProduct product, InAppPurchaseState state)
        {
            OnPurchaseComplete?.Invoke(new InAppPurchaseEventData(product, state));
        }

        private ProductType GetUnityIAPType(InAppPurchaseProductType inAppPurchaseProductType)
        {
            switch (inAppPurchaseProductType)
            {
                case InAppPurchaseProductType.Consumable:
                    return ProductType.Consumable;
                case InAppPurchaseProductType.NonConsumable:
                    return ProductType.NonConsumable;
                default:
                    return ProductType.Subscription;
            }
        }
    }
}
#endif