﻿using System;
using GameReady.IAP.Data;
using GameReady.IAP.Validator;

namespace GameReady.IAP.Core
{
    public class MockIAPListener : IIAPListener
    {
        public event Action<bool> OnInitialized;

        public event Action<InAppPurchaseEventData> OnPurchaseComplete;

        private InAppPurchaseProduct[] _products;
       
        public void Initialize(InAppPurchaseProduct[] products, BaseIAPValidator validator)
        {
            _products = products;
            OnInitialized?.Invoke(true);
        }

        public void Purchase(string storeId)
        {
            var product = Array.Find(_products, purchaseProduct => purchaseProduct.StoreID == storeId);
            OnPurchaseComplete?.Invoke(product == null
                ? new InAppPurchaseEventData(null, InAppPurchaseState.PurchaseFailed)
                : new InAppPurchaseEventData(product, InAppPurchaseState.PurchaseComplete));
        }

        public void Restore()
        {
            foreach (var product in _products)
            {
                OnPurchaseComplete?.Invoke(new InAppPurchaseEventData(product, InAppPurchaseState.PurchaseComplete));
            }
        }
    }
}