﻿using GameReady.Plugins.SimpleJSON;

namespace GameReady.IAP.Validator.Data
{
    public readonly struct ValidationRequestEventData
    {
        public readonly PurchaseResponse Response;

        public readonly JSONNode RawData;

        public ValidationRequestEventData(PurchaseResponse response, JSONNode rawData)
        {
            Response = response;
            RawData = rawData;
        }
    }
}