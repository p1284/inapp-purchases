﻿namespace GameReady.IAP.Validator.Data
{
    /// <summary>
    /// Parameters required for a server-side validation request.
    /// </summary>
    [System.Serializable]
    public class ReceiptRequest
    {
        public string store;
        public string bid;
        public string pid;
        public string type;
        public string user;
        public string receipt;
    }
}