﻿namespace GameReady.IAP.Validator.Data
{
    /// <summary>
    /// Response parameters received from a server-side validation request.
    /// </summary>
    [System.Serializable]
    public struct PurchaseResponse
    {
        public int status;
        public string type;
        public long expiresDate;
        public bool autoRenew;
        public bool billingRetry;
        public string productId;
        public bool sandbox;

        public override string ToString()
        {
            return
                $"ProductId:{productId}, Status:{status}, Type:{type}, ExpiresDate:{expiresDate}, AutoRenew:{autoRenew}, BillingRetry:{billingRetry}, Sandbox:{sandbox}";
        }
    }
}