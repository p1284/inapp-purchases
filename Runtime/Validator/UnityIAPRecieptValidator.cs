﻿#if UNITY_IAP
using System;
using Cysharp.Threading.Tasks;
using GameReady.IAP.Validator;
using GameReady.IAP.Validator.Data;
using GameReady.Plugins.SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

namespace Gameready.IAP.Validator
{
    [Serializable]
    public class FlobukValidatorSettings
    {
        public const string validationEndpoint = "https://flobuk.com/validator/v1/receipt/";
        public const string userEndpoint = "https://flobuk.com/validator/v1/user/";


        [SerializeField] private string _userId;
        [SerializeField] private string _appId;

        public string UserId => _userId;

        public string AppId => _appId;

        public string GetType(ProductType type)
        {
            switch (type)
            {
                case ProductType.Consumable:
                case ProductType.Subscription:
                    return type.ToString();

                default:
                    return "Non-Consumable";
            }
        }
    }

    public enum ValidationPurchaseState
    {
        Purchased,
        Pending,
        Failed
    }

    [CreateAssetMenu(menuName = "Create IAPValidator", fileName = "UnityIAPRecieptValidator")]
    public sealed class UnityIAPRecieptValidator : BaseIAPValidator
    {
        public event Action<ValidationRequestEventData> OnRemoteValidationComplete;

        private IStoreController _storeController;

        [SerializeField] private bool _useLocalValidation = true;
        [SerializeField] private bool _useRemoteFlobukValidation;

        [SerializeField] private FlobukValidatorSettings _flobukValidator;

        [SerializeField] private bool useAppleStoreKitTestCertificate;

        private CrossPlatformValidator _localValidator;

        public void Initialize(IStoreController storeController)
        {
            _storeController = storeController;
            if (UnityIAPValidationSupport.IsCurrentStoreSupportedByValidator())
            {
#if !UNITY_EDITOR
            var appleTangleData =
                useAppleStoreKitTestCertificate ? AppleStoreKitTestTangle.Data() : AppleTangle.Data();
            _localValidator =
                new CrossPlatformValidator(GooglePlayTangle.Data(), appleTangleData, Application.identifier);
#endif
            }
        }

        public ValidationPurchaseState ValidatePurchase(Product product)
        {
            //assume that the purchase is valid as default
            ValidationPurchaseState state = ValidationPurchaseState.Purchased;
            //running on unsupported store
            if (_storeController == null)
            {
                return state;
            }

            //nothing to validate without receipt
            if (!product.hasReceipt)
            {
                return ValidationPurchaseState.Failed;
            }

            //if local validation is supported it could return otherwise
            if (UnityIAPValidationSupport.IsCurrentStoreSupportedByValidator() && _localValidator != null)
            {
                state = _useLocalValidation ? LocalValidation(product) : ValidationPurchaseState.Purchased;
            }

            //local validation was not supported or it passed as valid
            //now do the server validation, if supported and keep transaction pending
            if (_useRemoteFlobukValidation && state == ValidationPurchaseState.Purchased &&
                UnityIAPValidationSupport.IsCurrentStoreSupportedByValidator())
            {
                RequestValidationAsync(product).Forget();
                return ValidationPurchaseState.Pending;
            }

            //return state of local validation, or default state
            //if no validation technique was supported at all
            return state;
        }

        private ValidationPurchaseState LocalValidation(Product product)
        {
            try
            {
                var result = _localValidator.Validate(product.receipt);

                foreach (var receipt in result)
                {
                    if (receipt is GooglePlayReceipt googleReceipt)
                    {
                        if ((int)googleReceipt.purchaseState == 2 || (int)googleReceipt.purchaseState == 4)
                        {
                            //deferred IAP, payment not processed yet
                            return ValidationPurchaseState.Pending;
                        }
                    }
                }

                LogReceipts(result);

                return ValidationPurchaseState.Purchased;
            }
            //if the purchase is deemed invalid, the validator throws an exception
            catch (IAPSecurityException)
            {
                return ValidationPurchaseState.Failed;
            }
        }

        private async UniTaskVoid RequestValidationAsync(Product product)
        {
            //if the app is closed during this time, ProcessPurchase will be
            //called again for the same purchase once the app is opened again
            JSONNode receiptData = JSON.Parse(product.receipt);
            string transactionID = receiptData["TransactionID"].Value;

#if UNITY_IOS
            IPurchaseReceipt[] receipts = localValidator.Validate(product.receipt);
            foreach (AppleInAppPurchaseReceipt receipt in receipts)
            {
                if (product.definition.id != receipt.productID)
                    continue;

                transactionID = receipt.transactionID;
                break;
            }
#endif

            ReceiptRequest request = new ReceiptRequest()
            {
                store = receiptData["Store"].Value,
                bid = Application.identifier,
                pid = product.definition.id,
                user = _flobukValidator.UserId,
                type = _flobukValidator.GetType(product.definition.type),
                receipt = transactionID
            };
            string postData = JsonUtility.ToJson(request);

            JSONNode rawResponse = null;
            using (var www =
                   UnityWebRequest.Put(FlobukValidatorSettings.validationEndpoint + _flobukValidator.AppId, postData))
            {
                www.SetRequestHeader("content-type", "application/json");
                await www.SendWebRequest();

                //raw JSON response
                rawResponse = JSON.Parse(www.downloadHandler.text);

                PurchaseResponse response = default;
                if (rawResponse.HasKey("data"))
                {
                    //  string productId = rawResponse["data"]["productId"].Value;
                    response = JsonUtility.FromJson<PurchaseResponse>(rawResponse["data"].ToString());

                    // if (inventory.ContainsKey(productId)) inventory[productId] = thisPurchase; //already exist, replace
                    // else inventory.Add(productId, thisPurchase); //add new to inventory
                }

                OnRemoteValidationComplete?.Invoke(new ValidationRequestEventData(response, rawResponse));

                // purchaseCallback?.Invoke(
                //     www.error == null && string.IsNullOrEmpty(rawResponse["error"]) && rawResponse.HasKey("data"),
                //     rawResponse);
            }

            //do not complete pending purchases but still leave them open for processing again later
            if (rawResponse == null || rawResponse.HasKey("error") && (int)rawResponse["code"] == 10130)
            {
                return;
            }

            //once we have done the validation in our backend, we update the purchase status
            _storeController.ConfirmPendingPurchase(product);
        }

        private void LogReceipts(IPurchaseReceipt[] receipts)
        {
            Debug.Log("Receipt is valid. Contents:");
            foreach (var receipt in receipts)
            {
                LogReceipt(receipt);
            }
        }

        private void LogReceipt(IPurchaseReceipt receipt)
        {
            Debug.Log($"Product ID: {receipt.productID}\n" +
                      $"Purchase Date: {receipt.purchaseDate}\n" +
                      $"Transaction ID: {receipt.transactionID}");

            if (receipt is GooglePlayReceipt googleReceipt)
            {
                Debug.Log($"Purchase State: {googleReceipt.purchaseState}\n" +
                          $"Purchase Token: {googleReceipt.purchaseToken}");
            }

            if (receipt is AppleInAppPurchaseReceipt appleReceipt)
            {
                Debug.Log($"Original Transaction ID: {appleReceipt.originalTransactionIdentifier}\n" +
                          $"Subscription Expiration Date: {appleReceipt.subscriptionExpirationDate}\n" +
                          $"Cancellation Date: {appleReceipt.cancellationDate}\n" +
                          $"Quantity: {appleReceipt.quantity}");
            }
        }
    }
}
#endif