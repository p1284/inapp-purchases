﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameReady.Analytics;
using GameReady.IAP.Core;
using GameReady.IAP.Data;
using UnityEngine;
using VContainer.Unity;
using Object = UnityEngine.Object;

namespace GameReady.IAP
{
    public sealed class IAPPurchaseManager : IInAppPurchaseManager, IDisposable,IAsyncStartable
    {
        public event Action<IAPPurchaseManager> OnInitialized;
        public event Action<InAppPurchaseEventData> OnPurchaseResult;

        private readonly IInappPurchaseManagerConfig _config;
        private readonly IAnalyticsManager _analytics;

        private bool _isInitialized;
        
        private IIAPListener _iapListener;

        private string _currentPlacement;

        public IAPPurchaseManager(IInappPurchaseManagerConfig config, IAnalyticsManager analyticsManager)
        {
            _config = config;
            _analytics = analyticsManager;
        }

        public bool IsInitialized => _isInitialized;


        public bool IsPurchased(string productId, out int count)
        {
            count = PlayerPrefs.GetInt(productId, 0);
            return count > 0;
        }

        public InAppPurchaseProduct[] GetAllProducts()
        {
            return _config.Products;
        }

        public InAppPurchaseProduct[] GetProducts(Predicate<InAppPurchaseProduct> predicate)
        {
            return Array.FindAll(_config.Products, predicate).ToArray();
        }

        public InAppPurchaseProduct GetProduct(Predicate<InAppPurchaseProduct> predicate)
        {
            foreach (var purchaseProduct in _config.Products)
            {
                if (predicate(purchaseProduct))
                {
                    return purchaseProduct;
                }
            }

            return null;
        }
        
        public async UniTask StartAsync(CancellationToken cancellation)
        {
            await UniTask.Yield();

#if UNITY_IAP

            var iapObject = new GameObject("UnityIAPListener", typeof(UnityIAPListener));
            Object.DontDestroyOnLoad(iapObject);

            _iapListener = iapObject.GetComponent<IIAPListener>();

            _iapListener.OnInitialized += OnIapInititalized;
            _iapListener.OnPurchaseComplete += OnPurchaseCompleteHandler;
            _iapListener.Initialize(GetAllProducts(), _config.RecieptValidator);

#else
            _iapListener = new MockIAPListener();
            _iapListener.OnInitialized += OnIapInititalized;
            _iapListener.OnPurchaseComplete += OnPurchaseCompleteHandler;
            _iapListener.Initialize(GetAllProducts(), _config.RecieptValidator);
#endif
        }
        
        private void OnPurchaseCompleteHandler(InAppPurchaseEventData eventData)
        {
            switch (eventData.State)
            {
                case InAppPurchaseState.PurchaseStarted:
                    eventData.PurchasedProduct.PurchaseStarted();
                    break;
                case InAppPurchaseState.PurchaseComplete:
                    BuyProduct(eventData.PurchasedProduct);
                    break;
                case InAppPurchaseState.PurchaseFailed:
                    eventData.PurchasedProduct?.PurchaseFailed();
                    break;
            }

            Log($"{eventData.State}");

            OnPurchaseResult?.Invoke(eventData);
        }

        private void BuyProduct(InAppPurchaseProduct product)
        {
            if (product.Type == InAppPurchaseProductType.NonConsumable && IsPurchased(product.ID, out var count))
            {
                product.Purchase(count);
                return;
            }

            var totalBought = PlayerPrefs.GetInt(product.ID, 0);
            PlayerPrefs.SetInt(product.ID, ++totalBought);

            product.Placement = _currentPlacement;
            product.Purchase(totalBought);

            if (_analytics != null && _iapListener is not MockIAPListener)
            {
                _analytics.TrackCustomEvent(_config.PurchaseAnalyticEvent.EventName, new Dictionary<string, object>()
                {
                    { "placement", _currentPlacement },
                    { "product_id", product.StoreID },
                    { "product_type", product.Type.ToString().ToLower() },
                    { "pay_id", product.PayoutID },
                }, _config.PurchaseAnalyticEvent.Filter);
            }

            Log($"Product:{product.ToString()} purchased");
        }

        private void OnIapInititalized(bool result)
        {
            if (result)
            {
                _iapListener.OnInitialized -= OnIapInititalized;
                //
                // foreach (var product in _products)
                // {
                //     if (IsPurchased(product.ID, out int count))
                //     {
                //         product.Purchase(count);
                //     }
                // }
            }

            _isInitialized = true;
            OnInitialized?.Invoke(this);
        }

        public void Purchase(InAppPurchaseProduct product, string placement = "")
        {
            Purchase(product.StoreID, placement);
        }

        public void Purchase(string storeId, string placement = "")
        {
            _currentPlacement = placement;
            _iapListener?.Purchase(storeId);
        }

        public void Restore()
        {
            _iapListener.Restore();
        }
        
        private void Log(string message)
        {
            Debug.Log($"<b>{GetType().Name}</b>: {message}");
        }

        public void Dispose()
        {
            _iapListener = null;
        }

        
    }
}