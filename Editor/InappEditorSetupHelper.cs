using UnityEditor;
using GameReady.EditorTools;
using GameReady.IAP.Data;

namespace GameReady.IAP.Editor
{
    public static class InappEditorSetupHelper
    {
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/InApp Purchases/GetOrCreate IAPPurchaseConfig", false, 0)]
        public static void GetOrCreateInAppConfig()
        {
            EditorHelpers.CreateScriptableAsset<IInappPurchaseManagerConfig>(true, true);
        }
#if CHEATS
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/InApp Purchases/Create Cheats", false, 0)]
        public static void CreateCheats()
        {
            EditorHelpers.CreateScriptableAsset<GameReady.IAP.Cheats.InappCheats>(true, true);
        }
#endif
    }
}