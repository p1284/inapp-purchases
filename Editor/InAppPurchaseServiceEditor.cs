﻿using GameReady.EditorTools;
using GameReady.IAP.Data;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace GameReady.IAP.Editor
{
    [CustomEditor(typeof(IInappPurchaseManagerConfig))]
    public class InAppPurchaseServiceEditor : UnityEditor.Editor
    {
        private const string UNITY_IAP = "UNITY_IAP";

        private const string UnityIAPPackage = "com.unity.purchasing";

        private IInappPurchaseManagerConfig _config;

        private VisualElement _container;

        private void OnEnable()
        {
            _config = (IInappPurchaseManagerConfig)target;
        }

        public override VisualElement CreateInspectorGUI()
        {
            _container = new VisualElement();

            InspectorElement.FillDefaultInspector(_container, serializedObject, this);

#if UNITY_IAP
            var btn = new Button(() =>
            {
                var validationRef = serializedObject.FindProperty("_recieptValidator");
                validationRef.objectReferenceValue =
                    EditorHelpers.CreateScriptableAsset<Gameready.IAP.Validator.UnityIAPRecieptValidator>(false, true);
                serializedObject.ApplyModifiedProperties();

                EditorUtility.SetDirty(_config);
                AssetDatabase.SaveAssets();
            });
            btn.text = "Create UnityIAPValidator";
            _container.Add(btn);
#endif

            var validateBtn = new Button(() => { _config.Validate(); });
            validateBtn.text = "Validate";
            _container.Add(validateBtn);

            validateBtn = new Button(() =>
            {
                _config.Validate();
                EditorUtility.SetDirty(_config);
                AssetDatabase.SaveAssets();
            });
            validateBtn.text = "Save";
            _container.Add(validateBtn);

            var checkBtn = new Button();

            checkBtn.name = "check_btn";
            checkBtn.text = "Check Unity IAP package";
            checkBtn.clicked += OnCheckButtonHandler;
            _container.Add(checkBtn);

            return _container;
        }

        private void OnCheckButtonHandler()
        {
            EditorHelpers.CheckPackageInstalled(UnityIAPPackage, result =>
            {
                _container.Q<Button>("check_btn").clicked -= OnCheckButtonHandler;
                _container.Q<Button>("check_btn").RemoveFromHierarchy();

                var btn = new Button();
                if (result)
                {
                    btn.name = "remove_iap";
                    btn.text = "Remove UnityIAP packages";
                    btn.clicked += RemoveIapClicked;
                }
                else
                {
                    btn.name = "install_iap";
                    btn.text = "Install UnityIAP packages";
                    btn.clicked += InstallIapClicked;
                }

                _container.Add(btn);
            });
        }

        private void InstallIapClicked()
        {
            _container.Q<Button>("install_iap").clicked -= InstallIapClicked;
            EditorHelpers.InstallPackage(UnityIAPPackage, b =>
            {
                if (b)
                {
                    EditorHelpers.AddDefineForCurrentPlatform(UNITY_IAP);
                }
            });
        }

        private void RemoveIapClicked()
        {
            _container.Q<Button>("remove_iap").clicked -= RemoveIapClicked;
            EditorHelpers.RemoveDefineForCurrentPlatform(UNITY_IAP);
            EditorHelpers.UninstallPackage(UnityIAPPackage, null);
        }
    }
}